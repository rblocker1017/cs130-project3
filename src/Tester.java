
public class Tester {

	public static void main(String[] args) {
		BSTNode n1 = new BSTNode(3);
		BSTNode n2 = new BSTNode(15);
		BSTNode n3 = new BSTNode(10, n1, n2);
		BST t1 = new BST(n1);
		
		System.out.println("Checking if t1 contains '3'");
		System.out.println(t1.contains(n3, 3));
		System.out.println("Checking if t1 contains '4'");
		System.out.println(t1.contains(n3, 4));
		System.out.println("Checking if t1 contains '5'");
		System.out.println(t1.contains(n3, 5));
		System.out.println("Checking if t1 contains '10'");
		System.out.println(t1.contains(n3, 10));
		System.out.println("Checking if t1 contains '15'");
		System.out.println(t1.contains(n3, 15));
		System.out.println("Checking if t1 contains '2321'");
		System.out.println(t1.contains(n3, 2321));
	}

}
