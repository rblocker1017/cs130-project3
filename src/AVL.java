
public class AVL extends BST {

	public AVL(AVLNode r) {
		super(r);
	}

	/* public AVLNode balance(AVLNode n) {
		if (n == null)
			return n;

		if (getHeight(n.left) - getHeight(n.right) > 1)
			if (getHeight(n.left.left) >= getHeight(n.left.right))
				n = singleLeftRotation(n);
			else
				n = doubleLeftRightRotation(n);
		else if (getHeight(n.right) - getHeight(n.left) > 1)
			if (getHeight(n.right.right) >= getHeight(n.right.left))
				n = singleRightRotation(n);
			else
				n = doubleRightLeftRotation(n);

		n.height = Math.max(getHeight(n.left), getHeight(n.right)) + 1;
		return n;
	} */

	public int getHeight(AVLNode n) {
		return n.height;
	}

	/*
	 * public AVLNode insert(AVLNode n, int x) { balance(super.insert(n, x)); }
	 * 
	 * private AVLNode singleRightRotation(AVLNode k2) { AVLNode k1 = k2.getLeft();
	 * k2.setLeft(k1.getRight()); k1.setRight(k2);
	 * k2.setHeight(Math.max(height(k2.getLeft()), height(k2.getRight())) + 1);
	 * k1.setHeight(Math.max(height(k1.getLeft()), k2.getHeight()) + 1);
	 * 
	 * return k1; }
	 * 
	 * private AVLNode singleLeftRotation(AVLNode k1) { AVLNode k2 = k1.getRight();
	 * k1.setRight(k2.getLeft()); k2.setLeft(k1);
	 * k1.setHeight(Math.max(height(k1.getLeft()), height(k1.getRight())) + 1);
	 * k2.setHeight(Math.max(height(k2.getRight()), k1.getHeight()) + 1);
	 * 
	 * return k2; }
	 * 
	 * public AVLNode doubleLeftRightRotation(AVLNode k3) {
	 * k3.setLeft(singleLeftRotation(k3.getLeft())); return singleRightRotation(k3);
	 * }
	 * 
	 * public AVLNode doubleRightLeftRotation(AVLNode k1) {
	 * k1.setRight(singleRightRotation(k1.getRight())); return
	 * singleLeftRotation(k1); }
	 */
}
