public class BST {

	BSTNode root;

	public BST() {
	}

	public BST(BSTNode r) {
		root = r;
	}

	public int compareTo(int x)
	{
		int a = 0;
		int b = 0; 
		int cmp = a > b ? +1 : a < b ? -1 : 0;
		return cmp;
	}
	
	public BSTNode insert(BSTNode b, int x) {
		if (b == null) {
			return new BSTNode(x, null, null);
		}

		if (b.value < x) {
			b.left = insert(b.left, x);
		} else if (b.value > x) {
			b.right = insert(b.right, x);
		} else {

		}

		return b;
	}

	public BSTNode delete(BSTNode b, int x) {
		if (b == null)
			return b; // Item not found; do nothing

		int compareResult = compareTo(b.value);

		if (compareResult < 0)
			b.left = delete(b.left, x);
		else if (compareResult > 0)
			b.right = delete(b.right, x);
		else if (b.left != null && b.right != null) // Two children
		{
			b.value = findMin(b.right).value;
			b.right = delete(b.right, b.value);
		} else
			b = (b.left != null) ? b.left : b.right;
		return b;
	}

	public boolean contains(BSTNode b, int x) {
		if (b == null)
			return false;

		if (x > b.value)
			return contains(b.right, x);
		else if (x < b.value)
			return contains(b.left, x);
		else
			return true;

		/*
		 * int compareResults = x.compareTo(b.value); if (compareResult < 0) return
		 * contains(x, b.left); else if (compareResult > 0) return contains(x, b.right);
		 * else return true; //match
		 */
	}

	BSTNode findMin(BSTNode n) { // finds the minimum node
		if (n != null) {
			while (n.left != null) // traverse the left side until you find null value
				n = n.left;
		}
		return n;
	}

	BSTNode findMax(BSTNode n) { // finds the maximum node
		if (n != null) {
			while (n.right != null) // traverse the right side until you find the null value
				n = n.right;
		}
		return n;
	}

}
