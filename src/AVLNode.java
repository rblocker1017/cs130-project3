
public class AVLNode extends BSTNode {
	int height;
	AVLNode left;
	AVLNode right;
	
	public AVLNode(int x, int h, AVLNode l, AVLNode r) {
		super(x, l, r);
		height = h;
	}
	
	public int getHeight(AVLNode n) {
		return n.height;
	}
}
