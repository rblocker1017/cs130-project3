
public class BSTNode 
{
	int value;
	BSTNode left;
	BSTNode right;

	public BSTNode() 
	{
		int value;
		BSTNode left;
		BSTNode right;
	}
	
	public BSTNode(int x)
	{
		value = x;
	}
	
	public BSTNode(int x, BSTNode l, BSTNode r)
	{
		value = x;
		left = l;
		right = r;
	}
}
